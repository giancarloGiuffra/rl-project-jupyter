import datetime as dt
import importlib
import json
import logging
import os
import pickle
import sys
from collections import Counter

import numpy as np
import pandas as pd
from tabulate import tabulate

from src.data_import import Importing as importing
from src.fqi.FQI import FQI
from src.models import IO as model_io
from src.policy.QPolicy import QPolicy
from src.rewards.Position import Position
from src.rewards.UnrealizedReward import UnrealizedReward
from src.utils import set_position, create_all_combination, is_parallelizable


def get_logger(configuration, time):
    name = 'main_script--{configuration}--{time}'.format(configuration=configuration,
                                                         time=time.strftime("%Y-%m-%d_%H-%M-%S"))
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    fh = logging.FileHandler('./logs/{name}'.format(name=name))
    fh.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='%(asctime)s [{config}] - %(message)s'.format(config=configuration),
                                  datefmt='%d/%m/%Y %I:%M:%S %p')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    logger.addHandler(ch)
    logger.addHandler(fh)
    return logger


def random_action(state):
    the_random_action = pd.Series(np.random.randint(-1, 2, state.shape[0]), dtype='category')
    return the_random_action.rename('action')


def possible_actions(integer_positions):
    return [Position(integer) for integer in integer_positions]


def main(running_parameters, starting_running_time, config_file):
    # logger
    logger = get_logger(os.path.splitext(config_file)[0], starting_running_time)

    # input dataset
    logger.info('Reading training set from file {file} ...'.format(file=running_parameters['input_dataset']['file']))
    file_data = importing.import_df(running_parameters['input_dataset']['file'])
    logger.info('...Done')

    logger.info('Applying talib functions to training set: {talib} ...'.format(
        talib=running_parameters['input_dataset']['talib_func']))
    file_data_talib, talib_names = importing.apply_talib(file_data,
                                                         running_parameters['input_dataset']['talib_func'])
    logger.info('...Done')

    logger.info('Creating training dataframes for dates {start} to {end} ...'
                .format(start=running_parameters['input_dataset']['start_date'],
                        end=running_parameters['input_dataset']['end_date']))
    current_state_no_position, next_state_no_position, price_info, minutes = \
        importing.create_tuples(
            running_parameters['input_dataset']['start_date'],
            running_parameters['input_dataset']['end_date'],
            file_data_talib,
            running_parameters['input_dataset']['historical_observations'],
            running_parameters['input_dataset']['columns_with_historical'],
            running_parameters['input_dataset']['columns_with_current'] + talib_names, running_parameters['input_dataset']['starting_time']
        )
    logger.info('...Done')

    action = random_action(current_state_no_position)
    current_state, next_state = set_position(current_state_no_position, next_state_no_position, action, minutes)

    # reward
    reward = UnrealizedReward(running_parameters['reward']['fee'],
                              running_parameters['reward']['position_size']).calculate(current_state,
                                                                                       action,
                                                                                       price_info,

                                                                                       minutes)

    # samples creation
    samples = {
        'current_state': current_state.copy(),
        'next_state': next_state.copy(),
        'reward': reward,
        'action': action,
        'minute': minutes,
        'fee': running_parameters['reward']['fee'],
        'position_size': running_parameters['reward']['position_size'],
        'price_info': price_info,
    }
    if running_parameters['fqi']['All_possible_tuples']:
        samples = create_all_combination(samples, possible_actions(running_parameters['fqi']['possible_actions']))

    # model
    logger.info('Initializing model {model} with parameters {parameters} ...'
                .format(model=running_parameters['model']['name'],
                        parameters=running_parameters['model']['parameters']))
    model_module = importlib.import_module(
        '.' + running_parameters['model']['name'],
        'src.models')
    model = model_module.get_model(running_parameters['model']['parameters'],
                                   samples['current_state'].copy(),
                                   samples['reward'],
                                   samples['action'])
    logger.info('...Done')

    # FQI
    fqi_configuration = {
        'possible_actions': possible_actions(running_parameters['fqi']['possible_actions']),
        'max_iterations': running_parameters['fqi']['max_iterations'],
        'discount': running_parameters['fqi']['discount'],
        'sample_iterations': 1 if running_parameters['fqi']['All_possible_tuples'] else running_parameters['fqi'][
            'sample_iterations'],
    }
    logger.info('Running FQI with parameters {parameters} ...'
                .format(parameters=fqi_configuration))
    if is_parallelizable(model):
        model.set_params(n_jobs=-1)
    fqi = FQI(samples, model, fqi_configuration, logger)
    fitted_model, q_norms, losses = fqi.run(**(running_parameters['model']['run_parameters']))
    logger.info('...Done')

    # optimal policy applied to training
    logger.info('Applying optimal policy to training set...')
    current_state_train = current_state_no_position.copy()
    current_state_train['position'] = Position.F
    if is_parallelizable(model):
        fitted_model.set_params(n_jobs=1)
    policy = QPolicy(fitted_model)
    optimal_state_train, optimal_actions_train = policy.apply(minutes,
                                                              current_state_train,
                                                              possible_actions(running_parameters['fqi']['possible_actions']))
    reward_train = UnrealizedReward(running_parameters['reward']['fee'],
                                    running_parameters['reward']['position_size']).calculate(optimal_state_train,
                                                                                             optimal_actions_train,
                                                                                             price_info,
                                                                                             minutes)
    logger.info('...Done')

    # validation
    logger.info('Reading validation set from file {file} ...'
                .format(file=running_parameters['validation_dataset']['file']))
    file_data_validation = importing.import_df(running_parameters['validation_dataset']['file'])
    logger.info('...Done')

    logger.info('Applying talib functions to validation set: {talib} ...'.format(
        talib=running_parameters['input_dataset']['talib_func']))
    file_data_validation_talib, talib_names = importing.apply_talib(file_data_validation,
                                                                    running_parameters['input_dataset']['talib_func'])
    logger.info('...Done')

    logger.info('Creating validation dataframes for dates {start} to {end} ...'
                .format(start=running_parameters['validation_dataset']['start_date'],
                        end=running_parameters['validation_dataset']['end_date']))
    current_state_validation, nex_state_validation, price_info_val, minutes_val = \
        importing.create_tuples(
            running_parameters['validation_dataset']['start_date'],
            running_parameters['validation_dataset']['end_date'],
            file_data_validation_talib,
            running_parameters['input_dataset']['historical_observations'],
            running_parameters['input_dataset']['columns_with_historical'],
            running_parameters['input_dataset']['columns_with_current'] + talib_names,
            running_parameters['input_dataset']['starting_time']
        )
    logger.info('...Done')

    logger.info('Applying optimal policy to validation set...')
    current_state_validation['position'] = Position.F
    if is_parallelizable(model):
        fitted_model.set_params(n_jobs=1)
    policy = QPolicy(fitted_model)
    optimal_state, optimal_actions = policy.apply(minutes_val,
                                                  current_state_validation,
                                                  possible_actions(running_parameters['fqi']['possible_actions']))
    logger.info('...Done')

    logger.info('Calculating optimal reward achieved in validation set...')
    reward_val = UnrealizedReward(running_parameters['reward']['fee'],
                                  running_parameters['reward']['position_size']).calculate(optimal_state,
                                                                                           optimal_actions,
                                                                                           price_info_val,
                                                                                           minutes_val)
    logger.info('...Done')

    # saving outputs
    if 'save_directory' in running_parameters and running_parameters['save_directory']:
        # saving model
        model_filename = os.path.join(running_parameters['save_directory'],
                                      '{model}--{configuration}--{time}'.format(
                                          model=running_parameters['model']['name'],
                                          configuration=os.path.splitext(config_file)[0],
                                          time=starting_running_time.strftime("%d-%m-%Y_%H-%M-%S")))
        logger.info('Saving model to {file} ...'
                    .format(file=model_filename))
        model_io.save(fitted_model, model_filename)
        logger.info('...Done')

        # saving other outputs
        outputs_filename = os.path.join(running_parameters['save_directory'],
                                        'outputs--{model}--{configuration}--{time}'.format(
                                            model=running_parameters['model']['name'],
                                            configuration=os.path.splitext(config_file)[0],
                                            time=starting_running_time.strftime("%d-%m-%Y_%H-%M-%S")))
        logger.info('Saving outputs to {file} ...'.format(file=outputs_filename))

        if is_parallelizable(model):
            feature_importance = model.feature_importances_
        else:
            feature_importance = np.zeros(current_state.shape[1] + 1)

        with open(outputs_filename, 'wb') as file:
            pickle.dump({'action': optimal_actions,
                         'reward': reward_val,
                         'feature_importance': feature_importance,
                         'feature_name': current_state_validation.columns,
                         'tot_reward_train': sum(reward_train),
                         'q_norms': q_norms,
                         'losses': losses,
                         'reward_training': reward_train,
                         'price_close_training': price_info['close'],
                         'price_close_validation': price_info_val['close'],
                         }, file)
        logger.info('...Done')

    # output
    colnames = current_state_validation.columns.to_list()
    colnames.append('action')
    logger.info('############################  Output  ############################')
    logger.info('Sum of optimal Reward(training set): {reward}'.format(reward=sum(reward_train)))
    logger.info('Sum of optimal Reward(validation set): {reward}'.format(reward=sum(reward_val)))
    logger.info('Sum of Reward(training set - random action): {reward}'.format(reward=sum(reward)))
    logger.info('Optimal action summary (training set): {summary}'.format(summary=Counter(optimal_actions_train)))
    logger.info('Optimal action summary (validation set): {summary}'.format(summary=Counter(optimal_actions)))

    logger.info('\n{table}'.format(table=tabulate([colnames, feature_importance.tolist()], headers='firstrow')))


if __name__ == "__main__":
    with open(sys.argv[1]) as configuration_file:
        parameters = json.load(configuration_file)
        starting_time = dt.datetime.now().replace(microsecond=0)
        main(parameters, starting_time, os.path.basename(configuration_file.name))
